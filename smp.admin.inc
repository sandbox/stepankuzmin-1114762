<?php

/**
  * @file 
  * Strobe Media Playback administration
  */

/**
 * system_settings_form
 */
function smp_admin() {
  $form['smp_path_to_player'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Strobe Media Playback'),
    '#description' => t('Path to StrobeMediaPlayback.swf'),
    '#required' => TRUE,
    '#default_value' => variable_get('smp_path_to_player', drupal_get_path('module', 'smp').'/StrobeMediaPlayback.swf'),
  );

  return system_settings_form($form);
}


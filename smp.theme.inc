<?php

/**
  * @file
  * Strobe Media Playback theme
  */

/**
  * Renders Strobe Media Playback
  * @param array $flash_variables
  * 
  * @return string $output
  */
function theme_strobe_media_playback($flash_variables) {
  $path_to_player = url(
    variable_get('smp_path_to_player', drupal_get_path('module', 'smp').'/StrobeMediaPlayback.swf'),
    array('absolute' => TRUE, 'alias' => TRUE)
  );

  // Create flash variables string
  foreach ($flash_variables as $key=>$value) {
    $variables_array[] = "$key=$value";
  }
  $variables_string = implode('&', $variables_array);

  // Generate output
  $output = "<object>
  <param name=\"movie\" value=\"$path_to_player\"></param>
  <param name=\"FlashVars\" value=\"$variables_string\"></param>
  <param name=\"allowFullScreen\" value="true"></param>
  <param name=\"allowscriptaccess\" value=\"always\"></param>
  <embed src=\"$path_to_player\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" FlashVars=\"$variables_string\">
  </embed></object>";

  return $output;
}
